/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package display;

/**
 *
 * @author Konrad Czarnota
 */
class DisplayViewModel {
    private String text;
    public DisplayViewModel() {
        text = "";
    }
    public String getText() {
        return this.text;
    } 
    public void setText(String nowyText) {
        this.text = nowyText;
    }
}
