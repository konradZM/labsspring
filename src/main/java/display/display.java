/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package display;

/**
 *
 * @author Konrad Czarnota
 */
import pk.labs.LabA.Contracts.interfaces.Display;
import display.DisplayViewModel;
import display.DisplayForm;
import javax.swing.*;

public class display implements Display{
    private DisplayForm window;
    private DisplayViewModel model;
    public display() {
        window = new DisplayForm();
        model = new DisplayViewModel();
    }
    public void setText(String text) {
        model.setText(text);
    }
    public JPanel getPanel() {
        return this.window;
    }
}
