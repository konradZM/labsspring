package pk.labs.LabA;
import pk.labs.LabA.Contracts.interfaces.*;

public class LabDescriptor {
    // region P1
    public static String displayImplClassName = "pk.labs.LabA.Implementation.Display";
    public static String controlPanelImplClassName = "pk.labs.LabA.Implementation.ControlPanel";

    public static String mainComponentSpecClassName = "pk.labs.LabA.Contracts.MainComponentSpec";
    public static String mainComponentImplClassName = "pk.labs.LabA.Implementation.MainComponent";
    // endregion

    // region P2
    public static String mainComponentBeanName = "...";
    public static String mainFrameBeanName = "...";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "...";
    // endregion
}
