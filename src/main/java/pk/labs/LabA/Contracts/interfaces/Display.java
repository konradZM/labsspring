package pk.labs.LabA.Contracts.interfaces;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//package pk.labs.LabA.Contracts;

import javax.swing.JPanel;


public interface Display {  
    JPanel getPanel();
    
    void setText(String text);
    
}
